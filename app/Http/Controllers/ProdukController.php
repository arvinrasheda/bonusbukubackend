<?php

namespace App\Http\Controllers;

use App\Produk;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;


class ProdukController extends Controller
{
    public function index()
    {
        if (session()->has('adminlogin')) {
            $produks = Produk::all();
            return view('admin.index',compact('produks'));
        }else{
            return redirect('adminlogin');
        }        
    }
    /**
     * Get ID PRODUK
     */
    // public function get_idproduk() {
    //     $today = date("Y-m-d");
    //     $tahun = date("Ymd");
    //     $kode = 'PRD';
    //     $query = DB::table('produks')->select('id')->where('tanggal',$today)->get();
    //     $max_id = $query->max('id');
    //     $max_id1 =(int) substr($max_id,11,3);
    //     $prdID = $max_id1 +1;
    //     $maxprdID = $tahun.$kode.sprintf("%03s",$prdID);
    //     return view('auth.login',compact('maxprdID'));
    //    }
    public function create()
    {

    }
    public function store(Request $request)
    {
        $produk = new Produk();
        $produk->nama_produk = $request->name;
        $produk->password_produk = md5($request->password);
        $produk->save();

        return redirect('produk')->with('success','Produk telah ditambahkan');
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        $bonuses = DB::table('bonuses')->where('id_produk',$id)->get();
        $produks = Produk::find($id);
        return view('admin.kelola',compact('produks','bonuses'));
    }
    public function update(Request $request, $id)
    {
        $produks = Produk::find($id);
        $produks->nama_produk = $request->nama;
        $produks->password_produk = md5($request->password);
        $produks->save();
        return redirect('produk')->with('success','Produk Telah Diperbarui');
    }
    public function destroy($id)
    {
        $produks = Produk::find($id);
        $bonuses = DB::table('bonuses')->where('id_produk',$id)->delete();
        $produks->delete();
        return redirect('produk')->with('success','Produk Telah Dihapus');
    }
}
