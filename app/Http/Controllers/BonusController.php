<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bonus;

class BonusController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    public function store(Request $request)
    {
        $data = new Bonus();
        $id_prod = $request->input('id_produk');
        if($request->get('vidio_upload')){
            $data->judul_bonus = $request->input('nama');
            $data->kategori_bonus = "Youtube Upload";
            $file = $request->file('file_vidio_upload');
            $ext = $file->getClientOriginalExtension();
            $newName = rand(100000,1001238912).".".$ext;
            $file->move('uploads/file',$newName);
            $data->path_bonus = $newName;
            $data->id_produk = $request->input('id_produk');
            $data->save();
        }elseif($request->get('vidio_youtube')){
            $data->judul_bonus = $request->input('nama');
            $data->kategori_bonus = "Youtube Link";
            $data->path_bonus = $request->input('link_yt');
            $data->id_produk = $request->input('id_produk');
            $data->save();
        }elseif($request->get('button_file_upload')){
            $data->judul_bonus = $request->input('nama');
            $data->kategori_bonus = "File Upload";
            $file = $request->file('file_upload');
            $ext = $file->getClientOriginalExtension();
            $newName = rand(100000,1001238912).".".$ext;
            $file->move('uploads/file',$newName);
            $data->path_bonus = $newName;
            $data->id_produk = $request->input('id_produk');
            $data->save();
        }else{
            $data->judul_bonus = $request->input('nama');
            $data->kategori_bonus = "File Link";
            $data->path_bonus = $request->input('link_file');
            $data->id_produk = $request->input('id_produk');
            $data->save();
        }
        return redirect('produk/'.$id_prod.'/edit')->with('success','Bonus telah ditambahkan');
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        //
    }
    public function update(Request $request, $id)
    {
        //
    }
    public function destroy(Request $request, $id)
    {
        $id_prod = $request->input('id_produk');

        $bonuses = Bonus::find($id);
        unlink('uploads/file/'.$bonuses->path_bonus);
        $bonuses->delete();
        return redirect('produk/'.$id_prod.'/edit')->with('success','Bonus Telah Dihapus');
    }
}
