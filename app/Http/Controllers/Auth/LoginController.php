<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    public function index (){
        if (session()->has('userlogin')) {
            return redirect('home');
        }else{
            return view('index');
        }
    }
    public function postLogin(Request $request){
        $kodebonus = md5($request->password_e);
        $data = DB::table('products')->where('password_produk', $kodebonus)->first();
        if(count($data) > 0){ //apakah password tersebut ada atau tidak
            if($kodebonus==$data->password_produk){
                session(['id_produk'=>$data->id]);
                session(['nama_produk'=>$data->nama_produk]);
                session(['userlogin'=>'login']);
                return redirect('home');
            }
            else{
                return redirect('/')->with('alert','Kode bonus yang anda masukkan, salah !');
            }
        }   
    }
    public function bonuslogout(){
        $sesi = session()->has('userlogin');
        if($sesi){
        session()->forget('userlogin');
        session()->flush();
    }
        return redirect('/');
    }
}
