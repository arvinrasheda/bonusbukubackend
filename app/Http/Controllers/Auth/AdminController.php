<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function index (){
        if (session()->has('adminlogin')) {
            return redirect('produk');
        }else{
            return view('login');
        }
    }
    public function postLogin(Request $request){
        $email = $request->email;
        $pass = md5($request->password);
        $data = DB::table('users')->where('password', $pass)->where('email', $email)->first();
        if(count($data) > 0){ //apakah user dan password tersebut ada atau tidak
            if($pass==$data->password){
                session(['id'=>$data->id]);
                session(['email'=>$data->email]);
                session(['nama'=>$data->nama]);
                session(['adminlogin'=>'login']);
                return redirect('produk')->with('success','Halo Admin, Selamat Datang Di Dashboard Admin Bonus !');
            }
            else{
                return redirect('/adminlogin')->with('alert','Kode bonus yang anda masukkan, salah !');
            }
        }
    }
    public function adminlogout(){
        $sesi = session()->has('adminlogin');
        if($sesi){
        session()->forget('adminlogin');
        session()->flush();
    }
        return redirect('adminlogin');
    }
}
