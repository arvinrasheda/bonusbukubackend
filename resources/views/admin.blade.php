<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Admin Panel</title>

	<!-- Styles -->
	<link rel="stylesheet" href="{{asset('css/app.css')}}">
	<link rel="stylesheet" href="{{asset('css/upload.css')}}">
	<!-- Fonts -->
	<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/feather.css')}}">
</head>

<body>
	<div class="page">
		<div class="page-main">
			@include('skin.header')
				@yield('content')
		</div>
		@include('skin.footer')
	</div>
	<script src="{{asset('js/app.js')}}"></script>
	<script src="{{asset('js/upload.js')}}"></script>
	<script src="{{asset('js/modal.js')}}"></script>
</body>

</html>
