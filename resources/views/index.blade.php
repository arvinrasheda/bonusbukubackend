@extends('user')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-6 offset-3 mx-auto">
      <div class="text-center mb-6">
        <img src="/image/logo-billionairestore.png" class="h-7" alt="">
      </div>
      <form class="card" action="{{ route('user.login.submit') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="card-body p-6">
          <div class="card-title">Masukkan Kode Bonus</div>
            @if (\Session::has('alert'))
            <div class="alert alert-danger">
            {{ \Session::get('alert') }}
            </div><br />
            @endif
          <div class="form-group">
            <div class="input-icon">
              <span class="input-icon-addon">
                <i class="fe fe-gift"></i>
              </span>
              <input type="password" name="password_e" class="form-control" placeholder="Kode Bonus" autofocus>
            </div>
          </div>
          <button type="submit" class="btn btn-primary btn-block">Ambil Bonus</button>
        </div>
      </form>
   </div>
  </div>
</div>
@endsection
