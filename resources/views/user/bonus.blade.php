@extends('user')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-12 mx-auto mt-5">
      <div class="card">
        <div class="card-status bg-blue"></div>
        <div class="card-header">
          <h3 class="card-title">Bonus Buku Login Instagram</h3>
          <div class="card-options">
              <a href="{{route('user.logout.submit')}}" class="btn btn-primary"><i class="fe fe-power"></i></a>
          </div>
        </div>
        <div class="card-body">
          <h3 class="mb-5">Video</h3>
          <div class="row">
            <div class="col-lg-12">
              <div class="card-columns">
                <div class="card p-3">
                  <a href="javascript:void(0)">
                    <iframe width="560" height="315" src="https://youtube.com/embed/2JSvv1hbdQg?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen class="embed-responsive"></iframe>
                  </a>
                  <div class="d-flex align-items-center mt-3">
                    <div>
                      <div class="font-weight-bold">Video Tutorial Cara Pengambilan Gambar Produk Yang Menjual Dari Kamera Profesional dan Kamera Gadget</div>
                      <small class="d-block text-muted">10 Juli 2018</small>
                    </div>
                  </div>
                </div>
                <div class="card p-3">
                  <a href="javascript:void(0)">
                    <iframe width="560" height="315" src="https://youtube.com/embed/8ylpENaEEUs?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen class="embed-responsive"></iframe>
                  </a>
                  <div class="d-flex align-items-center mt-3">
                    <div>
                      <div class="font-weight-bold">Video Tutorial Cara Mudah Mendapatkan Inspirasi Konten Menarik Di Instagram</div>
                      <small class="d-block text-muted">10 Juli 2018</small>
                    </div>
                  </div>
                </div>
                <div class="card p-3">
                  <a href="javascript:void(0)">
                    <img src="/image/bg.jpg" class="img-fluid">
                  </a>
                  <div class="d-flex align-items-center mt-3">
                    <div>
                      <div class="font-weight-bold">Video Tutorial Cara Mudah Mendapatkan Inspirasi Konten Menarik Di Instagram</div>
                      <small class="d-block text-muted">10 Juli 2018</small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <hr />
          <h3 class="mb-5">File</h3>
          <div class="row">
            <div class="col-lg-12">
              <div class="card-columns">
                <div class="card p-3">
                  <a href="javascript:void(0)" download>
                    <img src="/image/bg.jpg" class="img-fluid">
                  </a>
                  <div class="d-flex align-items-center mt-3">
                    <div>
                      <div class="font-weight-bold">Video Tutorial Cara Pengambilan Gambar Produk Yang Menjual Dari Kamera Profesional dan Kamera Gadget</div>
                      <small class="d-block text-muted">10 Juli 2018</small>
                    </div>
                  </div>
                </div>
                <div class="card p-3">
                  <a href="javascript:void(0)" download>
                    <img src="/image/bg.jpg" class="img-fluid">
                  </a>
                  <div class="d-flex align-items-center mt-3">
                    <div>
                      <div class="font-weight-bold">Video Tutorial Cara Mudah Mendapatkan Inspirasi Konten Menarik Di Instagram</div>
                      <small class="d-block text-muted">10 Juli 2018</small>
                    </div>
                  </div>
                </div>
                <div class="card p-3">
                  <a href="javascript:void(0)">
                    <img src="/image/bg.jpg" class="img-fluid">
                  </a>
                  <div class="d-flex align-items-center mt-3">
                    <div>
                      <div class="font-weight-bold">Video Tutorial Cara Mudah Mendapatkan Inspirasi Konten Menarik Di Instagram</div>
                      <small class="d-block text-muted">10 Juli 2018</small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="text-center text-muted mb-2">
        Copyright 2018
      </div>
    </div>
  </div>
</div>
@endsection
