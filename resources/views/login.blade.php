@extends('user')
@section('content')
<div class="container">
  <div class="row">
    <div class="col col-login mx-auto">
      <div class="text-center mb-6">
        <img src="/image/logo-billionairestore.png" class="h-7" alt="">
      </div>
      <form class="card" action="{{route('admin.login.submit')}}" method="post">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="card-body p-6">
          <div class="card-title">Login to your account</div>
          <div class="form-group">
            <label class="form-label">Email</label>
            <div class="input-icon">
              <span class="input-icon-addon">
                <i class="fe fe-user"></i>
              </span>
              <input type="email" name="email" class="form-control" placeholder="Email" autofocus>
            </div>
          </div>
          <div class="form-group">
            <label class="form-label">Password</label>
            <div class="input-icon">
              <span class="input-icon-addon">
                <i class="fe fe-lock"></i>
              </span>
              <input type="password" name="password" class="form-control" placeholder="Password">
            </div>
          </div>
          <div class="form-footer">
                <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
        </div>
      </form>
   </div>
  </div>
</div>
@endsection
