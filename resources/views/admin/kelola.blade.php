@extends('admin')
@section('content')
<div class="container my-3 my-md-5">
    <div class="page-header">
        <h1 class="page-title">
            {{$produks->nama_produk}}
        </h1>
    </div>
    @if (\Session::has('success'))
    <div class="alert alert-success">
       {{ \Session::get('success') }}
    </div><br />
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-status bg-blue"></div>
                <div class="card-header">
                    <h3 class="card-title">Daftar Bonus</h3>
                    <div class="card-options">
                        <button data-toggle="modal" data-target="#addbonus" class="btn btn-primary btn-sm"><i class="fe fe-plus-square mr-1"></i>Add
                            Bonus</button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-vcenter text-nowrap">
                        <thead>
                            <tr>
                                <th class="w-1">No.</th>
                                <th>Bonus</th>
                                <th>Kategori</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @php(
                                $no = 1 {{-- buat nomor urut --}}
                            )
                            @foreach($bonuses as $b)
                            <tr>
                                <td><span class="text-muted">{{ $no++ }}</span></td>
                                <td>{{$b->judul_bonus}}</td>
                                <td>{{$b->kategori_bonus}}</td>
                                <td>
                                    <button data-toggle="modal" data-target="#editbonus" class="btn btn-success btn-sm"><i
                                            class="fe fe-edit mr-1"></i>Edit</button>
                                    <button data-toggle="modal" id="hapusmodalbonus" data-id="{{ $b->id }}" data-target="#hapusbonus" class="btn btn-danger btn-sm"><i
                                            class="fe fe-trash mr-1"></i>Delete</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal fade" id="addbonus" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add Bonus</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form action="{{url('bonus')}}" enctype="multipart/form-data" method="POST">
                            @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="form-label">Nama Bonus</label>
                                <input type="text" class="form-control" name="nama" placeholder="Nama Bonus">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Kategori Bonus</label>
                                <div class="selectgroup w-100">
                                    <label class="selectgroup-item">
                                        <input type="radio" name="vidio_upload" id="1" value="1" class="selectgroup-input">
                                        <span class="selectgroup-button">Video/Upload</span>
                                    </label>
                                    <label class="selectgroup-item">
                                        <input type="radio" name="vidio_youtube" id="2" value="2" class="selectgroup-input">
                                        <span class="selectgroup-button">Video/Youtube</span>
                                    </label>
                                </div>
                                <div class="selectgroup w-100">
                                    <label class="selectgroup-item">
                                        <input type="radio" name="button_file_upload" id="3" value="3" class="selectgroup-input">
                                        <span class="selectgroup-button">File/Upload</span>
                                    </label>
                                    <label class="selectgroup-item">
                                        <input type="radio" name="file_link" id="4" value="4" class="selectgroup-input">
                                        <span class="selectgroup-button">File/Link</span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group" id="upload_video">
                                <label class="form-label">Video/Upload</label>
                                <div class="form-upload">
                                    <input type="file" name="file_vidio_upload">
                                    <div class="caption-upload text-center">
                                        <i class="fa fa-cloud-upload fa-3x text-primary mb-2"></i>
                                        <p class="font-weight-bold">Upload file .mp4 anda <span class="text-primary">di
                                                sini</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="yt_video">
                                <label class="form-label">Video/YouTube</label>
                                <div class="input-icon">
                                    <span class="input-icon-addon">
                                        <i class="fe fe-link"></i>
                                    </span>
                                    <input type="text" name="link_yt" class="form-control" placeholder="Link Youtube">
                                </div>
                            </div>
                            <div class="form-group" id="upload_file">
                                <label class="form-label">File/Upload</label>
                                <div class="form-upload">
                                    <input type="file" name="file_upload">
                                    <div class="caption-upload text-center">
                                        <i class="fa fa-cloud-upload fa-3x text-primary mb-2"></i>
                                        <p class="font-weight-bold">Upload file anda <span class="text-primary">di sini</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="link_file">
                                <label class="form-label">File/Link</label>
                                <div class="input-icon">
                                    <span class="input-icon-addon">
                                        <i class="fe fe-link"></i>
                                    </span>
                                    <input type="text" name="link_file" class="form-control" placeholder="Link File">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" name="id_produk" value="{{$produks->id}}">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Add</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="editbonus" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Edit Bonus</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="form-label">Nama Bonus</label>
                                <input type="text" class="form-control" name="" placeholder="Nama Bonus" value="Video Tutorial Cara Mudah Mendapatkan Inspirasi Konten Menarik Di Instagram">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Kategori Bonus</label>
                                <div class="selectgroup w-100">
                                    <label class="selectgroup-item">
                                        <input type="radio" name="kategori" id="5" value="1" class="selectgroup-input">
                                        <span class="selectgroup-button">Video/Upload</span>
                                    </label>
                                    <label class="selectgroup-item">
                                        <input type="radio" name="kategori" id="6" value="2" class="selectgroup-input">
                                        <span class="selectgroup-button">Video/Youtube</span>
                                    </label>
                                </div>
                                <div class="selectgroup w-100">
                                    <label class="selectgroup-item">
                                        <input type="radio" name="kategori" id="7" value="3" class="selectgroup-input">
                                        <span class="selectgroup-button">File/Upload</span>
                                    </label>
                                    <label class="selectgroup-item">
                                        <input type="radio" name="kategori" id="8" value="4" class="selectgroup-input">
                                        <span class="selectgroup-button">File/Link</span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group" id="edit_upload_video">
                                <label class="form-label">Video/Upload</label>
                                <div class="form-upload">
                                    <input type="file" name="file">
                                    <div class="caption-upload text-center">
                                        <i class="fa fa-cloud-upload fa-3x text-primary mb-2"></i>
                                        <p class="font-weight-bold">Upload file .mp4 anda <span class="text-primary">di
                                                sini</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="edit_yt_video">
                                <label class="form-label">Video/YouTube</label>
                                <div class="input-icon">
                                    <span class="input-icon-addon">
                                        <i class="fe fe-link"></i>
                                    </span>
                                    <input type="text" name="link_yt" class="form-control" placeholder="Link Youtube">
                                </div>
                            </div>
                            <div class="form-group" id="edit_upload_file">
                                <label class="form-label">File/Upload</label>
                                <div class="form-upload">
                                    <input type="file" name="file">
                                    <div class="caption-upload text-center">
                                        <i class="fa fa-cloud-upload fa-3x text-primary mb-2"></i>
                                        <p class="font-weight-bold">Upload file anda <span class="text-primary">di sini</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="edit_link_file">
                                <label class="form-label">File/Link</label>
                                <div class="input-icon">
                                    <span class="input-icon-addon">
                                        <i class="fe fe-link"></i>
                                    </span>
                                    <input type="text" name="link_file" class="form-control" placeholder="Link File">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Add</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="hapusbonus" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Hapus Bonus</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form id="proseshapusbonus" method="post">
                                @csrf
                                <div class="modal-body" id="konfirmasibonus">

                                </div>
                                <div class="modal-footer">
                                    <input type="hidden" name="id_produk" value="{{$produks->id}}">
                                    <input name="_method" type="hidden" value="DELETE">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
