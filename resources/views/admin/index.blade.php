@extends('admin')
@section('content')
<div class="container my-3 my-md-5">
    @if (\Session::has('success'))
    <div class="alert alert-success">
       {{ \Session::get('success') }}
    </div><br />
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Daftar Produk</h3>
                    <div class="card-options">
                        <button data-toggle="modal" data-target="#addproduk" class="btn btn-primary btn-sm"><i class="fe fe-plus-square mr-1"></i>Add
                            Produk</button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table card-table table-vcenter text-nowrap">
                        <thead>
                            <tr>
                                <th class="w-1">No.</th>
                                <th>Produk</th>
                                <th class="w-1">Aksi</th>
                                <th class="w-1"></th>
                                <th class="w-1"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @php(
                            $no = 1 {{-- buat nomor urut --}}
                            )
                            @foreach($produks as $p)
                            <tr>
                                <td><span class="text-muted">{{ $no++ }}</span></td>
                                <td>{{$p->nama_produk}}</td>
                                <td>
                                    <a href="{{action('ProdukController@edit', $p['id'])}}" class="btn btn-primary btn-sm">
                                        <i class="fe fe-settings mr-1"></i>Kelola
                                    </a></td>
                                <td><button data-toggle="modal" id="edit" data-id="{{ $p->id }}" data-name="{{ $p->nama_produk }}" data-password="{{ $p->password_produk }}" data-target="#editproduk" class="btn btn-success btn-sm"><i
                                            class="fe fe-edit mr-1"></i>Edit</button></td>
                                <td><button data-toggle="modal" id="hapus" data-id="{{ $p->id }}" data-target="#hapusproduk" class="btn btn-danger btn-sm"><i
                                    class="fe fe-trash mr-1"></i>Hapus</button></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal fade" id="addproduk" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add Produk</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form action="{{url('produk')}}" enctype="multipart/form-data" method="POST">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="form-label">Nama Produk</label>
                                    <input type="text" class="form-control" name="name" placeholder="Nama Produk">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Password Bonus Produk</label>
                                    <input type="password" class="form-control" name="password" placeholder="Password Bonus Produk">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="editproduk" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add Produk</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form id="prosesupdate" enctype="multipart/form-data" method="POST">
                            @csrf
                            <div class="modal-body">
                                    <div class="form-group">
                                            <label class="form-label">ID</label>
                                            <input type="text" class="form-control" id="id_produk" name="id_produk_e" placeholder="ID Produk" readonly>
                                        </div>
                                <div class="form-group">
                                    <label class="form-label">Nama Produk</label>
                                    <input type="text" class="form-control" id="nama_produk" name="nama" placeholder="Nama Produk">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Ganti Password Baru Produk</label>
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Password Baru Produk">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input name="_method" type="hidden" value="PUT">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="hapusproduk" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Hapus Bonus</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form id="proseshapus" method="post">
                            @csrf
                            <div class="modal-body" id="konfirmasi">

                            </div>
                            <div class="modal-footer">
                                <input name="_method" type="hidden" value="DELETE">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
