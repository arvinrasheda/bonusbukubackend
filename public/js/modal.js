    $(document).on('click', '#edit', function (e) {
     var id =$(this).attr('data-id');
     var getUrl = window.location;
     var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
     document.getElementById('id_produk').value = id;
     document.getElementById('nama_produk').value = $(this).attr('data-name')
    //  document.getElementById('password_produk').value = $(this).attr('data-password')
     $('#prosesupdate').attr("action",baseUrl+"/"+id);
    });
    $(document).on('click', '#hapus', function (e) {
        var id = $(this).attr('data-id');
        var getUrl = window.location;
        var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
        $('#konfirmasi').empty().append("Ingin Menghapus Produk "+id+" ?");
        $('#proseshapus').attr("action",baseUrl+"/"+id);
    });
    $(document).on('click', '#hapusmodalbonus', function (e) {
        var id = $(this).attr('data-id');
        $('#konfirmasibonus').empty().append("Ingin Menghapus Bonus "+id+" ?");
        $('#proseshapusbonus').attr("action","http://localhost:8000/bonus/"+id);
    });
