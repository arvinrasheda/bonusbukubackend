<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//==================================================================================
//ADMIN LOGIN
//==================================================================================
Route::get('/adminlogin', 'Auth\AdminController@index')->name('admin.login');
Route::post('/adminlogin', 'Auth\AdminController@postLogin')->name('admin.login.submit');
Route::get('/adminlogout', 'Auth\AdminController@adminlogout')->name('admin.logout.submit');
Route::get('/adminarea', 'ProdukController@index')->name('admin.home');
//==================================================================================
//USER LOGIN
//==================================================================================
Route::get('/', 'Auth\loginController@index')->name('user.login');
Route::post('/bonuslogin', 'Auth\loginController@postLogin')->name('user.login.submit');
Route::get('/bonuslogout', 'Auth\loginController@bonuslogout')->name('user.logout.submit');
Route::get('/home', 'UserController@index')->name('user.home');
// ==================================================================================
Route::resource('produk','ProdukController');
Route::resource('bonus','BonusController');
// Route::get('/id', 'ProdukController@get_idproduk');
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return redirect('/');
});
